const moment = require('moment')
const express = require('express')
const app = express()
const path = require('path')
const { execSync } = require('child_process')

const public = path.join(__dirname, "public")
const file = path.join(__dirname, 'bell-1.mp3')

console.log({ public })

app.use(express.json())
app.use(express.static(public))

app.post("/", (req, res) => {
    if (!moment().isBetween(moment("07:00", 'HH:mm'), moment("22:00", 'HH:mm'))) {
        return res.status(403).json({
            message: 'Já está tarde! Estamos dormindo... 😴😴😴',
            waitSeconds: 60
        })
    }
    res.json({
        message: "Tudo certo! Aguarda um pouco ai! 😎✌",
        waitSeconds: 15
    })
    const { name } = req.body
    playBell(name)
})

function playBell(name) {
    for (let i = 0; i < 2; i++) {
        console.log(`Tocando... (${i})`)
        execSync(`mpg123  -b 1000 ${file}`)
        if (name) {
            execSync(`espeak -v pt -s 140 "${encodeURI(name)}, está, na porta"`)
        }
    }
}

app.listen(process.env.PORT || 80, () => {
    console.log('server started')
})