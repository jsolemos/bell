import React, { useState, useEffect } from 'react';
import axios from 'axios'

import './App.css';

function App() {

  const [msg, setMessage] = useState()
  const [count, setCount] = useState(0)
  const [name, setName] = useState(null)

  useEffect(() => {
    const countStart = parseInt(localStorage.getItem('count'))
    if (countStart && countStart > 0) {
      startCountDown(countStart)
    }
  }, [])

  async function startCountDown(countDown = 15) {
    for (let i = countDown; i >= 0; i--) {
      setCount(i)
      localStorage.setItem('count', i)
      await new Promise(x => setTimeout(x, 1000))
    }
  }

  async function callBell() {
    setMessage("Tocando... aguarda aí!")
    axios.post("/", {
      name
    }).then(res => {
      console.log(res.data)
      setMessage(res.data.message)
      startCountDown(res.data.waitSeconds)
    })
      .catch(err => {
        console.error(err.response)
        setMessage(err.response?.status === 403 ? err.response?.data.message : "Ops!! Não rolou! tenta novamente ai ou liga pelo telefone! ☹☹")
        startCountDown(err.response?.data.waitSeconds)
      })
  }

  return (
    <div className="App">
      <header className="App-header">
        <h2>{msg}</h2>
        <div className="who-block">
          <input className="input-name" onChange={(e) => setName(e.target.value)} name="name" placeholder="Digite seu nome aqui." />
        </div>
        <button
          onClick={() => callBell()}
          disabled={count > 0}
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Tocar campainha
          <br />
          {count > 0 && <small>aguarde {count}s</small>}
        </button>
      </header>
    </div>
  );
}

export default App;
